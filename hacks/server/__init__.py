from flask import Flask, request, jsonify, render_template
from hacks.hack1 import amazonutil, forms as hack1_forms
import os

THIS_DIR = os.path.dirname(__file__)
TEMPLATES_DIR = os.path.join(THIS_DIR, 'templates')
STATIC_DIR = os.path.join(THIS_DIR, 'static')

app = Flask('hacks.server')
app.static_folder = STATIC_DIR
app.template_folder = TEMPLATES_DIR


@app.route("/")
def index():
    return "Make me do stuff..."


@app.route("/asin/<asin>")
def asin(asin):
        if request.args.get("format", "") == "xml":
            return amazonutil.AWSASINRequest(asin).response_xml()
        return jsonify(amazonutil.AWSASINRequest(asin).detail_dict())
