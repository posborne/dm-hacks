from flask_wtf import Form
from wtforms import TextField
from wtforms.validators import DataRequired


class AsinForm(Form):
    asin = TextField('Asin',
                     [DataRequired()])
