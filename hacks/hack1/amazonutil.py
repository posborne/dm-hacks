from flask import Response
from hacks.hack1 import config as h1_config
from bs4 import BeautifulSoup
from urlparse import urlparse
import base64
import datetime
import hashlib
import hmac
import requests
import urllib2


class AWSProductAPIRequest(object):
    def __init__(self, item_id, operation, resp_group):
        self.base_url = "http://webservices.amazon.com/onca/xml?"
        self.image_base_url = "https://images-na.ssl-images-amazon.com/images/I/"
        self.image_suffix = "._SL200_.jpg"
        self.item_id = item_id
        self.operation = operation
        self.resp_group = resp_group
        # Add these to config
        self.access_key_id = h1_config.AWS_PRODUCT_ACCESS_KEY_ID
        self.secret_access_key = h1_config.AWS_PRODUCT_SECRET_ACCESS_KEY
        self.associate_tag = h1_config.AWS_PRODUCT_ASSOCIATE_TAG

    def _make_request(self):
        stamp = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        query_str = ("AWSAccessKeyId=" + self.access_key_id +
                     "&AssociateTag=" + self.associate_tag +
                     "&ItemId=" + self.item_id +
                     "&Operation=" + self.operation +
                     "&ResponseGroup=" + self.resp_group +
                     "&Service=AWSECommerceService" +
                     "&Timestamp=" + stamp +
                     "&Version=2011-08-01")
        query_str = self._escape_query_str(query_str)
        signature = self._create_signature(query_str)
        request_url = self.base_url + query_str + "&Signature=" + signature
        resp = requests.get(request_url)
        return resp.text

    def image_url(self, xml):
        tmp_url = xml.ItemLookupResponse.Items.Item.SmallImage.URL.string
        a = urlparse(tmp_url).path
        return self.image_base_url + a[a.rfind("/") + 1:a.find(".")] + self.image_suffix

    def convert_amt(self, amount):
        if len(amount) == 1:
            return "0.0" + amount
        elif len(amount) == 2:
            return "0." + amount
        else:
            return amount[:-2] + "." + amount[-2:]

    def response_xml(self):
        resp_text = self._make_request()
        return Response(resp_text, mimetype="text/xml")

    def detail_dict(self):
        try:
            xml = BeautifulSoup(self._make_request(), ["lxml", "xml"])
            if xml.Errors:
                return dict(success=False, asin=self.item_id,
                            message="Amazon ASIN lookup failed.",
                            detail=xml.Errors.Error.Message.string)
            if int(xml.Offers.TotalOffers.string) == 0:
                return dict(success=False, asin=self.item_id,
                            message="This item is not available for purchase " \
                            "from Amazon currently. Sorry for the inconvenience.")
            if xml.ListPrice:
                price = xml.ListPrice.Amount.string
            else:
                price = xml.OfferListing.Price.Amount.string
            d = dict(success=True,
                     asin=xml.ASIN.string,
                     title=xml.Title.string,
                     product_url=urllib2.unquote(xml.DetailPageURL.string),
                     product_image_url=self.image_url(xml),
                     price=self.convert_amt(price),
                     offer_price=self.convert_amt(xml.OfferListing.Price.Amount.string),
                     ss_shipping_elig=(True if xml.IsEligibleForSuperSaverShipping.string == "1" else False))
            return d
        except Exception as exception:
            return dict(success=False, asin=self.item_id,
                        message="Amazon ASIN lookup failed with errors.",
                        exception=exception.__class__.__name__)

    def _escape_query_str(self, query_str):
        return (query_str
                .replace(":", "%3A")
                .replace(",", "%2C"))

    def _create_signature(self, query_str):
        msg = "GET\nwebservices.amazon.com\n/onca/xml\n" + query_str
        dig = hmac.new(self.secret_access_key, msg, hashlib.sha256).digest()
        return urllib2.quote(base64.b64encode(dig).decode(), safe="")


class AWSASINRequest(AWSProductAPIRequest):
    def __init__(self, item_id):
        super(AWSASINRequest, self).__init__(item_id,
                                             "ItemLookup",
                                             "ItemAttributes,Images,Offers")
