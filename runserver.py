def main():
    from hacks.server import app
    app.run(host='0.0.0.0', port=5001, debug=True)

if __name__ == '__main__':
    main()
